package com.agero.nccsdk.lbt.network;

import android.content.Context;

import com.agero.nccsdk.BuildConfig;
import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.TrackingContext;
import com.agero.nccsdk.domain.data.NccMotionData;
import com.agero.nccsdk.domain.data.NccSensorData;
import com.agero.nccsdk.internal.common.statemachine.SdkConfig;
import com.agero.nccsdk.internal.common.statemachine.SdkConfigType;
import com.agero.nccsdk.internal.common.util.AppUtils;
import com.agero.nccsdk.internal.common.util.DeviceUtils;
import com.agero.nccsdk.internal.data.memory.ClientCache;
import com.agero.nccsdk.internal.data.network.aws.kinesis.KinesisClient;
import com.agero.nccsdk.internal.data.network.aws.model.LbtPayload;
import com.agero.nccsdk.internal.data.preferences.ConfigManager;
import com.agero.nccsdk.internal.data.preferences.NccSharedPrefs;
import com.agero.nccsdk.lbt.config.KinesisConfig;
import com.agero.nccsdk.lbt.util.DataUtils;
import com.agero.nccsdk.lbt.util.TimedOperation;
import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
public class LbtDataManagerTest {

    private LbtDataManager lbtDataManager;

    @Mock
    private KinesisClient mockSensorKinesisClient;

    @Mock
    private NccSharedPrefs mockSharedPrefs;

    @Mock
    private ClientCache mockClientCache;

    @Mock
    private TimedOperation mockKinesisUploadTimer;

    @Mock
    private ConfigManager mockConfigManager;

    @Mock
    private Context mockContext;

    @Before
    public void setUp() throws Exception {
        lbtDataManager = new LbtDataManager(mockSensorKinesisClient, mockSharedPrefs, mockClientCache, mockKinesisUploadTimer, mockConfigManager);
    }

    @Test
    @PrepareForTest({NccSdk.class, DeviceUtils.class, AppUtils.class})
    public void streamData_valid_nccsensordata() throws Exception {
        NccSensorType sensorType = NccSensorType.MOTION_ACTIVITY;
        List<NccSensorData> testData = new ArrayList<>();
        testData.add(new NccMotionData(1234567890, 1, 50));
        testData.add(new NccMotionData(1234567891, 2, 75));
        String testUserId = "userId";

        PowerMockito.mockStatic(NccSdk.class);
        PowerMockito.when(NccSdk.class, "getApplicationContext").thenReturn(mockContext);
        PowerMockito.when(mockSharedPrefs, "getUserId").thenReturn(testUserId);

        PowerMockito.mockStatic(DeviceUtils.class);
        PowerMockito.when(DeviceUtils.class, "getDeviceName").thenReturn("deviceName");

        PowerMockito.mockStatic(AppUtils.class);
        PowerMockito.when(AppUtils.class, "getAppVersion", mockContext).thenReturn("1.0");

        TrackingContext trackingContext = new TrackingContext();
        trackingContext.putDouble("d", 1.0);
        trackingContext.putString("s", "string");
        trackingContext.putLong("l", 1234567890);
        trackingContext.putBoolean("b", true);
        trackingContext.putFloat("f", 1.234567f);
        trackingContext.putInt("i", 1);
        PowerMockito.when(mockClientCache.getTrackingContext()).thenReturn(trackingContext);

        LbtPayload testPayload = new LbtPayload(testUserId, sensorType.getShortIdentifier(), DataUtils.toJSONArrayString(testData));
        testPayload.setMetadata(trackingContext.getData());
        String serializedPayload = new Gson().toJson(testPayload);

        lbtDataManager.streamData(sensorType, testData);

        verify(mockSensorKinesisClient).save(serializedPayload, BuildConfig.KINESIS_STREAM_LBT);
    }

    @Test
    public void streamData_null_nccsensordata() throws Exception {
        NccSensorType sensorType = NccSensorType.ACCELEROMETER;
        List<NccSensorData> testData = null;
        lbtDataManager.streamData(sensorType, testData);
        verifyNoMoreInteractions(mockSensorKinesisClient);
    }

    @Test
    @PrepareForTest({ConfigManager.class})
    public void onStart() throws Exception {
        SdkConfigType testConfigType = SdkConfigType.KINESIS;
        SdkConfig testConfig = new KinesisConfig(1000);
        when(mockConfigManager.getSdkConfig(testConfigType)).thenReturn(testConfig);

        lbtDataManager.onStart();
        verify(mockConfigManager).getSdkConfig(testConfigType);
        verify(mockKinesisUploadTimer).start(((KinesisConfig) testConfig).getUploadInterval());
    }

    @Test
    public void onStop() throws Exception {
        lbtDataManager.onStop();
        verify(mockKinesisUploadTimer).stop();
    }

}
