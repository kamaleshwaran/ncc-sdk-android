package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccLocationData;
import com.agero.nccsdk.domain.data.NccSensorData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Created by james hermida on 12/5/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class NccLocationMapperTest {

    private AbstractSensorDataMapper mapper = new NccLocationMapper();

    @Test
    public void map() throws Exception {
        long timeUtc = 1315314000000L;
        String formattedDate = mapper.getReadableTimestamp(timeUtc);
        double lat = 12.3456;
        double lng = 78.9012;
        double altitude = 10;
        float course = 45f;
        float accuracy = 100f;
        double distance = 100;
        float speed = 10f;
        NccSensorData data = new NccLocationData(timeUtc, lat, lng, altitude, course, accuracy, speed, distance);

        String expected = "G," + lat + "," + lng + "," + altitude + "," + course + "," + accuracy + ",NA," + speed + "," + distance + ",NA," + timeUtc + "," + formattedDate;
        String actual = mapper.map(data);

        assertEquals(expected, actual);
    }
}
