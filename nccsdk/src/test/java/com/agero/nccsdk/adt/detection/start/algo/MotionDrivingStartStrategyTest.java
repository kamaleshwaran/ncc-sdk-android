package com.agero.nccsdk.adt.detection.start.algo;

import android.content.Context;

import com.agero.nccsdk.adt.AdtTriggerType;
import com.agero.nccsdk.domain.data.NccMotionData;
import com.google.android.gms.location.DetectedActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by james hermida on 11/30/17.
 */

@RunWith(PowerMockRunner.class)
public class MotionDrivingStartStrategyTest {

    @Mock
    private Context mockContext;

    private int testThreshold = 25;
    private MotionDrivingStartStrategy motionCollectionStartStrategy;

    @Before
    public void setUp() throws Exception {
        motionCollectionStartStrategy = PowerMockito.spy(new MotionDrivingStartStrategy(mockContext, testThreshold));
    }

    @Test
    public void evaluate_null_data() throws Exception {
        motionCollectionStartStrategy.evaluate(null);

        verify(motionCollectionStartStrategy, times(0)).sendCollectionStartBroadcast(any(AdtTriggerType.class));
    }

    @Test
    public void evaluate_starts_collection_confidence_matches_threshold() throws Exception {
        PowerMockito.doNothing().when(motionCollectionStartStrategy, "sendCollectionStartBroadcast", any(AdtTriggerType.class));

        NccMotionData data = new NccMotionData(0, DetectedActivity.IN_VEHICLE, testThreshold);
        motionCollectionStartStrategy.evaluate(data);
        verify(motionCollectionStartStrategy).sendCollectionStartBroadcast(AdtTriggerType.START_MOTION);
    }

    @Test
    public void evaluate_starts_collection_confidence_above_threshold() throws Exception {
        PowerMockito.doNothing().when(motionCollectionStartStrategy, "sendCollectionStartBroadcast", any(AdtTriggerType.class));

        NccMotionData data = new NccMotionData(0, DetectedActivity.IN_VEHICLE, testThreshold + 10);
        motionCollectionStartStrategy.evaluate(data);

        verify(motionCollectionStartStrategy).sendCollectionStartBroadcast(AdtTriggerType.START_MOTION);
    }

    @Test
    public void evaluate_invalid_activity() throws Exception {
        NccMotionData data = new NccMotionData(0, DetectedActivity.RUNNING, testThreshold);
        motionCollectionStartStrategy.evaluate(data);

        verify(motionCollectionStartStrategy, times(0)).sendCollectionStartBroadcast(AdtTriggerType.START_MOTION);
    }

    @Test
    public void evaluate_invalid_confidence() throws Exception {
        NccMotionData data = new NccMotionData(0, DetectedActivity.IN_VEHICLE, testThreshold - 10);
        motionCollectionStartStrategy.evaluate(data);

        verify(motionCollectionStartStrategy, times(0)).sendCollectionStartBroadcast(AdtTriggerType.START_MOTION);
    }
    
}
