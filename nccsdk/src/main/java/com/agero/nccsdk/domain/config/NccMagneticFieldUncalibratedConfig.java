package com.agero.nccsdk.domain.config;

/**
 * Created by james hermida on 8/30/17.
 */

public class NccMagneticFieldUncalibratedConfig extends NccAbstractNativeConfig {

    public NccMagneticFieldUncalibratedConfig() {
        super();
    }

    public NccMagneticFieldUncalibratedConfig(int samplingRate) {
        super(samplingRate);
    }
}
