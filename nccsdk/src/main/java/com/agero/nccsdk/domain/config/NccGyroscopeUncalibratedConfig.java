package com.agero.nccsdk.domain.config;

/**
 * Created by james hermida on 8/30/17.
 */

public class NccGyroscopeUncalibratedConfig extends NccAbstractNativeConfig {

    public NccGyroscopeUncalibratedConfig() {
        super();
    }

    public NccGyroscopeUncalibratedConfig(int samplingRate) {
        super(samplingRate);
    }
}
