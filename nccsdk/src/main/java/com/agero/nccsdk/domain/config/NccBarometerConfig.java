package com.agero.nccsdk.domain.config;

/**
 * Created by james hermida on 8/30/17.
 */

public class NccBarometerConfig extends NccAbstractNativeConfig {

    public NccBarometerConfig() {
        super();
    }

    public NccBarometerConfig(int samplingRate) {
        super(samplingRate);
    }
}
