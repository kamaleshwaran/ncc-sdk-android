package com.agero.nccsdk.internal.di.module;

import android.content.Context;

import com.agero.nccsdk.internal.common.io.FileCompressor;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.common.util.Clock;
import com.agero.nccsdk.internal.data.network.aws.s3.FileTransferManager;
import com.agero.nccsdk.internal.di.qualifier.SdkLocalFilesParentDirectory;
import com.agero.nccsdk.internal.di.qualifier.SdkLocalFilesPath;
import com.agero.nccsdk.internal.di.qualifier.UbiDataPath;
import com.agero.nccsdk.ubi.UbiInactiveState;
import com.agero.nccsdk.ubi.UbiState;
import com.agero.nccsdk.ubi.UbiStateMachine;
import com.agero.nccsdk.ubi.collection.io.NccFileCompressor;
import com.agero.nccsdk.ubi.network.UbiDataManager;
import com.agero.nccsdk.ubi.network.UbiSyncManager;

import java.io.File;
import java.util.concurrent.Executor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by james hermida on 2/26/18.
 */

@Module
public class UbiModule {

    @Provides
    @Singleton
    StateMachine<UbiState> provideUbiStateMachine(Context context) {
        return new UbiStateMachine(new UbiInactiveState(context));
    }

    @Provides
    @Singleton
    UbiSyncManager provideUbiSyncManager(Context context, Executor executor, Clock clock, FileTransferManager fileTransferManager, @UbiDataPath File dataPath, StateMachine<UbiState> ubiStateMachine) {
        return new UbiDataManager(context, executor, clock, fileTransferManager, dataPath, ubiStateMachine);
    }

    @Provides
    @SdkLocalFilesPath
    String provideSdkLocalFilesPathString(Context context) {
        return context.getFilesDir().getPath();
    }

    @Provides
    @Singleton
    FileCompressor provideFileCompressor() {
        return new NccFileCompressor();
    }

    @Provides
    @Singleton
    @UbiDataPath
    File providePostDataDirectory(@SdkLocalFilesPath String sdkLocalFilesPath, @SdkLocalFilesParentDirectory String sdkLocalFilesParentDirectory) {
        return new File(sdkLocalFilesPath + File.separator + sdkLocalFilesParentDirectory + File.separator + "p");
    }
}
