package com.agero.nccsdk.internal.log;

/**
 * Created by james hermida on 11/21/17.
 */

public enum LogLevel {

    /**
     * Maps to Log.v.
     */
    VERBOSE(2, "VERBOSE"),

    /**
     * Maps to Log.d.
     */
    DEBUG(3, "DEBUG"),

    /**
     * Maps to Log.i.
     */
    INFO(4, "INFO"),

    /**
     * Maps to Log.w.
     */
    WARN(5, "WARN"),

    /**
     * Maps to Log.e.
     */
    ERROR(6, "ERROR"),

    ASSERT(7, "ASSERT");

    private final int id;
    private final String name;

    LogLevel(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
