package com.agero.nccsdk.internal.common.io;

import java.io.File;
import java.io.IOException;

/**
 * Created by james hermida on 10/26/17.
 */

public interface FileWriterWrapper {

    void appendLine(final String s) throws IOException;

    void close() throws IOException;

    File getFile();

}
